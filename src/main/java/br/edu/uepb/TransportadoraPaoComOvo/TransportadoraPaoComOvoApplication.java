package br.edu.uepb.TransportadoraPaoComOvo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransportadoraPaoComOvoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransportadoraPaoComOvoApplication.class, args);
	}

}
